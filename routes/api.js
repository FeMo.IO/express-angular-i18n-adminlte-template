/*
 * Serve JSON to our AngularJS client
 */

var router = require('express').Router();

router.get('/name', function (req, res, next) {
    res.json({
        name: 'Bob'
    })
});

/*exports.name = function (req, res) {
  res.json({
  	name: 'Bob'
  });
};*/

module.exports = router;