/*
 * GET home page.
 */
var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
  res.render('index');
});

router.get('/:name', function (req, res, next) {
  res.render('partials/' + req.params.name, function (err, html) {
    if(err) {
      throw {status: 404, message: 'Page could not be found'}
    }
    res.render('index');
  })
});

router.get('/partials/:name', function (req, res, next) {
  res.render('partials/' + req.params.name, function (err, html) {
    if(err) {
      throw {status: 404, message: 'Page could not be found'}
    }
    res.send(html);
  })
});

/*exports.index = function(req, res){
  res.render('index');
};

exports.partials = function (req, res) {
  var name = req.params.name;
  res.render('partials/' + name);
};*/

module.exports = router;