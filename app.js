
/**
 * Module dependencies
 */

var express = require('express'),
  http = require('http'),
  path = require('path');

var app = module.exports = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var i18n = require('i18next');
var i18nMiddleware = require('i18next-express-middleware');
var i18nBackend = require('i18next-node-fs-backend');
var sprintf = require('i18next-sprintf-postprocessor');
var config = require('config');

var database = config.get('database');
var auth = config.get('auth');

var session = require('express-session');
const mongoStore = require('connect-mongo')(session);

i18n.use(i18nBackend)
  .use(i18nMiddleware.LanguageDetector)
  .use(sprintf)
  .init({
    saveMissing: true,
    saveMissingTo: 'current',
    preload: ['dev'],
    load: 'languageOnly',
    backend: {
      loadPath: 'locales/{{lng}}/{{ns}}.json',
      addPath: 'locales/{{lng}}/{{ns}}.missing.json',
      jsonIndent: 2
    },
    lng: 'de',
    debug: false
  });

var moment = require('moment');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
//var hbs = require('express-handlebars');

/*var hbsEngine = hbs.create({
  extname: 'hbs',
  defaultLayout: '../layout.hbs',
  helpers: {
    format: function (date, format) {
      return moment.utc(date).format(format);
    }
  }
});*/

/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
//app.engine('hbs', hbsEngine.engine);
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
    secret: "session-secret",
    store: new mongoStore({
        url: 'mongodb://localhost/' + database
    }),
    resave: false,
    saveUninitialized: true
}));
app.use(i18nMiddleware.handle(i18n));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    if(req.session) {
        if(!req.session.user && req.url != '/login') {
            //TODO perform redirect
            if(auth) {
                res.redirect("/login");
                return;
            }
        }
    }
    next();
});


/**
 * Routes
 */

// serve index and view partials
/*app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

// JSON API
app.get('/api/name', api.name);*/

var index = require('./routes/index');
var api = require('./routes/api');

app.use('/', index);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// Socket.io Communication
io.sockets.on('connection', require('./routes/socket'));

/**
 * Start Server
 */

server.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
