'use strict';

// Declare app level module which depends on filters, and services

angular.module('myApp', [
  'ui.router',

  'myApp.controllers',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',

  // 3rd party dependencies
  'btford.socket-io'
]).
config(function ($stateProvider, $locationProvider, $urlRouterProvider) {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.when('/', '/partial1');
  $stateProvider
      .state('view1', {
        url: '/partial1',
        templateUrl: '/partials/partial1',
        controller: 'MyCtrl1'
      })
      .state('view2', {
        url: '/partial2',
        templateUrl: '/partials/partial2',
        controller: 'MyCtrl2'
      })
});
